# GitLab CI template for Postman

This project implements a GitLab CI/CD template to run your automated (API) tests with [Postman](https://www.postman.com/automated-testing).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/postman'
    ref: '3.1.1'
    file: '/templates/gitlab-ci-postman.yml'

# Pipeline steps
stages:
  - acceptance # required by Postman template
  # TODO: add all other required stages
```

## `postman` job

This job starts [Postman automated tests](https://www.postman.com/automated-testing).

It uses the following variable:

| Name                  | description                              | default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `POSTMAN_IMAGE`       | The Docker image used to run Postman CLI. | `registry.hub.docker.com/postman/newman:latest` |
| `POSTMAN_COLLECTIONS` | The matcher to select Postman collection file(s) to run. | `postman/*collection.json` |
| `POSTMAN_EXTRA_ARGS`  | Newman extra [run options](https://github.com/postmanlabs/newman#command-line-options) (to use global variables, an environment or a data source for e.g.) | _none_ |
| `REVIEW_ENABLED`      | Set to `true` to enable Postman tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/postman-*.xunit.xml` | [JUnit](https://github.com/postmanlabs/newman#junitxml-reporter) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### Postman `{{base_url}}` auto evaluation

By default, the Postman template auto-evaluates a [{{base_url}} variable](https://learning.postman.com/docs/postman/variables-and-environments/variables/)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Postman test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Postman tests.
